from __future__ import print_function

import sys
import os
import imp
import zipfile
from random import random, randint

GSTPy = imp.load_dynamic('GSTPy', '/home/paul/dev/gst-php-build/lib/GSTPy.so')

#################### param section ######
user = "paul"
password = "paul"
host = "localhost"
port = 5432
db = "GSTweb"

fc_name = "gta"
template_file_path = "/var/www/html/gst-web/webgui/templates/crossSection_template_example.svg"
base_path = "/home/paul/dev/hackathon/generated_data/"
#########################################


def extractZIP(ret, output_path, target, content, middle):
    zip_ref = zipfile.ZipFile(output_path + ret, 'r')
    zip_ref.extractall(output_path)
    zip_ref.close()
    os.remove(output_path + ret)
    os.rename(output_path + content + ".shx",
              output_path + target + "_" + middle + ".shx")
    os.rename(output_path + content + ".shp",
              output_path + target + "_" + middle + ".shp")
    os.rename(output_path + content + ".prj",
              output_path + target + "_" + middle + ".prj")
    os.rename(output_path + content + ".dbf",
              output_path + target + "_" + middle + ".dbf")


def createCrossSection(ni, x_min, x_max, y_min, y_max, srs, feature_ids, output_path, out_shape_params, out_image_params, needed_points):

    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    steps_x = 1
    steps_y = 1
    ## we want exactly 20 borhole along the section
    bores_inbetween_min = 20
    bores_inbetween_max = 20
    vec_x = x_max - x_min
    vec_y = y_max - y_min
    import math
    len_vec = math.sqrt(vec_x*vec_x+vec_y+vec_y)

    for step_x in xrange(0, steps_x):
        for step_y in xrange(0, steps_y):

            start_p_x = x_min + step_x/steps_x * vec_x
            start_p_y = y_min + step_y/steps_y * vec_y
            end_p_x = x_min + (step_x + 1) / steps_x * vec_x
            end_p_y = y_min + (step_y + 1) / steps_y * vec_y

            target = str(step_x).zfill(4) + "_" + str(step_y).zfill(4)

            local_vec_x = end_p_x - start_p_x
            local_vec_y = end_p_y - start_p_y

            for cur_bore in xrange(0, randint(bores_inbetween_min, bores_inbetween_max)):
                cur_pos_x = start_p_x + random() * local_vec_x
                cur_pos_y = start_p_y + random() * local_vec_y
                bore_point_cur = GSTPy.Point2d(cur_pos_x, cur_pos_y)
                bore_point_cur_params = GSTPy.BoreHoleImageParameters(feature_ids, bore_point_cur, srs)
                try:
                    ret = ni.save_borehole_image(bore_point_cur_params, out_image_params)
                    os.rename(output_path + ret.intersection_image_file_name,
                              output_path + target + "_b_" + str(cur_bore) + ".svg")
                except:
                    pass
                try:
                    ret = ni.save_borehole_shape(bore_point_cur_params, out_shape_params)
                    extractZIP(ret, output_path, target, "Borehole_points", "b_" + str(cur_bore))
                except:
                    pass

            cur_bore = 0
            for p in needed_points:
                bore_point_cur = GSTPy.Point2d(x_min+p*vec_x, y_min+p*vec_y)
                bore_point_cur_params = GSTPy.BoreHoleImageParameters(feature_ids, bore_point_cur, srs)
                try:
                    ret = ni.save_borehole_image(bore_point_cur_params, out_image_params)
                    os.rename(output_path + ret.intersection_image_file_name,
                              output_path + target + "_saltdomeHit_" + str(cur_bore) + ".svg")
                except:
                    pass
                try:
                    ret = ni.save_borehole_shape(bore_point_cur_params, out_shape_params)
                    extractZIP(ret, output_path, target, "Borehole_points", "saltdomeHit_" + str(cur_bore))
                    cur_bore = cur_bore + 1
                except:
                    pass

            bore_point_start = GSTPy.Point2d(start_p_x, start_p_y)
            bore_point_end = GSTPy.Point2d(end_p_x, end_p_y)
            section_line = [
                GSTPy.Point2d(start_p_x, start_p_y), GSTPy.Point2d(end_p_x, end_p_y)
            ]

            bore_params_start = GSTPy.BoreHoleImageParameters(feature_ids, bore_point_start, srs)
            bore_params_end = GSTPy.BoreHoleImageParameters(feature_ids, bore_point_end, srs)
            section_params = GSTPy.SectionImageParameters(feature_ids, section_line, srs)

            try:
                ret = ni.save_borehole_image(bore_params_start, out_image_params)
                os.rename(output_path + ret.intersection_image_file_name, output_path + target + "_start_well.svg")
            except:
                pass
            try:
                ret = ni.save_borehole_shape(bore_params_start, out_shape_params)
                extractZIP(ret, output_path, target, "Borehole_points", "start_well")
            except:
                pass
            try:
                ret = ni.save_borehole_image(bore_params_end, out_image_params)
                os.rename(output_path + ret.intersection_image_file_name, output_path + target + "_end_well.svg")
            except:
                pass
            try:
                ret = ni.save_borehole_shape(bore_params_end, out_shape_params)
                extractZIP(ret, output_path, target, "Borehole_points", "end_well")
            except:
                pass
            try:
                ret = ni.save_cross_section_image(section_params, out_image_params)
                os.rename(output_path + ret.intersection_image_file_name, output_path + target + "_section.svg")
            except:
                pass
            try:
                ret = ni.save_cross_section_shape(section_params, out_shape_params)
                extractZIP(ret, output_path, target, "CrossSection_lines", "section")
            except:
                pass

            metadata = open(output_path + target + "_metadata.txt", "w")
            metadata.write(str(start_p_x) + "\n")
            metadata.write(str(start_p_y) + "\n")
            metadata.write(str(end_p_x) + "\n")
            metadata.write(str(end_p_y) + "\n")




ni = GSTPy.get_pg_connection(user, password, host, port, db)
ni.connect()

fc = next((fc for fc in ni.list_feature_classes()
           if fc.name == fc_name), None)
if fc is None:
    print("no feature class named \"" + fc_name + "\" found.")
    sys.exit(1)

features = ni.list_features(fc)
x_min = sys.float_info.max
y_min = sys.float_info.max
x_max = sys.float_info.min
y_max = sys.float_info.min

feature_ids = []
for f in features:
    x_min = min(x_min, f.bbox.xmin)
    y_min = min(y_min, f.bbox.ymin)
    x_max = max(x_max, f.bbox.xmax)
    y_max = max(y_max, f.bbox.ymax)
    feature_ids.append(f.feature_id)

srs = fc.srs

## take this srs EPSG:31469 so we can pick from the web
srs = GSTPy.SRS.from_gstsrs_id(20005)

#first cross section
x_min = [5018321, 5019044]
x_max = [5044346, 5053398]
y_min = [5930617, 5931372]
y_max = [5907571, 5912255]

## these points are pretty close but they certainly hit a saltdome
needed_points = [[0.14, 0.52, 0.91],
                [0.5]]

for i in xrange(0, len(x_min)):
    output_path = base_path + str(i) + "/"
    out_image_params = GSTPy.OutputImageParameters(
            output_path,
            template_file_path=template_file_path,
            image_type=GSTPy.ImageType.SVG)
    out_shape_params = GSTPy.OutputShapeParameters(output_path)

    createCrossSection(ni, x_min[i], x_max[i], y_min[i], y_max[i], srs,
                   feature_ids, output_path, out_shape_params, out_image_params, needed_points[i])



