#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 11:47:48 2017

@author: huang
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from library import *

class krig_class:
    def krig(self, GE_train, GE_test, Res_train, Res_test, cf_func, r, sill, nugget):
        sampled_var = sill*np.var(Res_train)
        sampled_mean = np.mean(Res_train)
    
        prediction, krig_var = kriging(GE_test, GE_train, Res_train
                           , r, sampled_mean, sampled_var, cf_func, nugget)    
    
        return r2_score(Res_test, prediction), prediction, krig_var
    
    def ord_krig(self, GE_train, GE_test, Res_train, Res_test, cf_func, r, sill, nugget):
        sampled_var = sill*np.var(Res_train)
        sampled_mean = np.mean(Res_train)
    
        prediction, krig_var = ordinary_kriging(GE_test, GE_train, Res_train
                           , r, sampled_mean, sampled_var, cf_func, nugget)    
    
        return r2_score(Res_test, prediction), prediction, krig_var
    
    def aniso_krig(self, GE_train, GE_test, Res_train, Res_test, cf_func, r, sill
                   , nugget, weight):
        sampled_var = sill*np.var(Res_train)
        sampled_mean = np.mean(Res_train)
    
        prediction, krig_var = aniso_kriging(GE_test, GE_train, Res_train
                           , r, sampled_mean, sampled_var, cf_func, nugget, weight)    
    
        return r2_score(Res_test, prediction), prediction, krig_var

    def variogram(self, GE_train, Res_train, r, cf_func, lags, tol, sill, nugget):
        sampled_var = sill*np.var(Res_train)
        cf = []
        for lag in lags:
            cf.append(sampled_var - cf_func(lag, r, sampled_var, nugget))
       
        r_h = experimental_vario(Res_train, GE_train, lags, tol)
        
        plt.figure()
        ax = plt.subplot(111)
        ax.plot(lags, r_h, 'o', label = 'variogram')
        ax.plot(lags, cf, '--', color='k', label = 'model')
        ax.set_title('Variogram fit')
        ax.set_ylabel('semi-variance')
        ax.set_xlabel('lags')
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        
    
    def aniso_variogram(self, GE_train, Res_train, r, cf_func, lags, tol, sill
                        , nugget, weight):
        sampled_var = sill*np.var(Res_train)
        cf = []
        for lag in lags:
            cf.append(sampled_var - cf_func(lag, r, sampled_var, nugget))
       
        r_h = aniso_vario(Res_train, GE_train, lags, tol, weight)
        
        plt.figure()
        plt.plot(lags, r_h, 'o')
        plt.plot(lags, cf, '--', color='k')
        plt.title('Variogram fit')
        plt.ylabel('semi-variance')
        plt.xlabel('lags')
        plt.legend(['variogram', 'model'])