# -*- coding: utf-8 -*-
"""
Created on Sun Sep 18 04:19:04 2016

@author: huang
"""
import numpy as np
from scipy.spatial.distance import cdist

def cf_gaussian(h, r, var_val, nugget):
    return var_val*np.exp(-3.0*(h/r)**2) - nugget

def cf_linear(h, r, var_val, nugget):
    return var_val*(1.0-h) - nugget

def cf_power(h, r, var_val, nugget):
    return var_val*(1.0-h)**2 - nugget
    

def cf_spherical (h, r, var_val, nugget):
    """Calculate covariance for a given lag and range on the basis of a spherical model
    
    **Arguments**:
        h: lag
        r: range
        var_val: variance
    """    
    # YOUR CODE HERE
    if(h<=r):
        return var_val - var_val * (1.5*h/r - 0.5*(h/r)**3) -nugget
    
    return 0

def cf_exponential (h, r, sample_var, nugget):
    '''Calculate covariance for a given lag and range on the basis of a exponential model
    
    **Arguments**:
    h : lag
    r : covariance range
    sample_var : variance of sample population
    '''
    # YOUR CODE HERE
    return sample_var*np.exp(-3.0*h/r) - nugget
    



def kriging(pos_unknown, pos, vals, r, sample_mean, sample_var, cf, nugget):
    '''Performs simple kriging with multiple points
    
    **Arguments**:
    pos_unknown: positions of unknown value, 2-D array
    ex: [[x1,y1,z1],[x2,y2,z2]] or [[x1],[x2],[x3]]
    pos : numpy array with positions of known values (used for interpolation)
    vals : numpy array corresponding property values at known positions
    r : variogram range
    sample_mean : mean value of sample population
    sample_var : variance of sample population
    cf: covariance function
    
    **Returns**:
    estimated_val, estimated_var : estimated value and variance, 1-D array
    '''
    # YOUR CODE HERE
    dim = len(pos)    
    dim2 = len(pos_unknown)    
    
    k = np.zeros(dim)          #vector k C10, C20, ...Cn0
    K = np.zeros((dim, dim))   #Covariance Matrix
    estimated_val = np.zeros(dim2)
    estimated_var = np.zeros(dim2)    
    
    #construct the K covariance matrix, only need to construct it once!    
    dist = cdist(pos, pos, 'euclidean')    
    
    for i in range(dim):
        j = i+1
        while(j<dim):
            K[i][j] = cf(dist[i][j], r, sample_var, nugget)
            K[j][i] = K[i][j]
            j+=1    
        K[i][i] = cf(0, r, sample_var, nugget)
    
    for j, p in enumerate(pos_unknown):
        dist = cdist([p], pos, 'euclidean')[0]    
        for i in range(dim):
            k[i] = cf(dist[i], r, sample_var, nugget)    
        w = np.linalg.solve(K, k)
        estimated_val[j] = np.dot(w, vals-sample_mean) + sample_mean
        estimated_var[j] = sample_var - np.dot(w, k)
        
    return estimated_val, estimated_var
    
def ordinary_kriging(pos_unknown, pos, vals, r, sample_mean, sample_var, cf, nugget):
    '''Performs ordinary kriging with multiple points
    
    **Arguments**:
    pos_unknown: positions of unknown value, 2-D array
    ex: [[x1,y1,z1],[x2,y2,z2]] or [[x1],[x2],[x3]]
    pos : numpy array with positions of known values (used for interpolation)
    vals : numpy array corresponding property values at known positions
    r : variogram range
    sample_mean : mean value of sample population
    sample_var : variance of sample population
    cf: covariance function
    
    **Returns**:
    estimated_val, estimated_var : estimated value and variance, 1-D array
    '''
    # YOUR CODE HERE
    dim = len(pos)+1
    dim2 = len(pos_unknown)    
    
    k = np.zeros(dim)          #vector k C10, C20, ...Cn0
    K = np.zeros((dim, dim))   #Covariance Matrix
    estimated_val = np.zeros(dim2)
    estimated_var = np.zeros(dim2)    
    
    k[dim-1] = 1.0             #ordinary kriging vector
    for i in range(dim-1):     #ordinary kriging matrix
        K[i][dim-1] = 1.0
        K[dim-1][i] = 1.0

    #construct the K covariance matrix, only need to construct it once!    
    dist = cdist(pos, pos, 'euclidean')    
    
    for i in range(dim-1):
        j = i+1
        while(j<dim-1):
            K[i][j] = cf(dist[i][j], r, sample_var, nugget)
            K[j][i] = K[i][j]
            j+=1    
        K[i][i] = cf(0, r, sample_var, nugget)
    
    for j, p in enumerate(pos_unknown):
        dist = cdist([p], pos, 'euclidean')[0]    
        for i in range(dim-1):
            k[i] = cf(dist[i], r, sample_var, nugget)    
        w = np.linalg.solve(K, k)
        estimated_val[j] = np.dot(w[:dim-1], vals)
        estimated_var[j] = sample_var - np.dot(w, k)
        
    return estimated_val, estimated_var
 
def aniso_kriging(pos_unknown, pos, vals, r, sample_mean, sample_var, cf, 
                  nugget, weight):
    '''Performs anisotropic kriging with multiple points
    
    **Arguments**:
    pos_unknown: positions of unknown value, 2-D array
    ex: [[x1,y1,z1],[x2,y2,z2]] or [[x1],[x2],[x3]]
    pos : numpy array with positions of known values (used for interpolation)
    vals : numpy array corresponding property values at known positions
    r : variogram range
    sample_mean : mean value of sample population
    sample_var : variance of sample population
    cf: covariance function
    
    **Returns**:
    estimated_val, estimated_var : estimated value and variance, 1-D array
    '''
    # YOUR CODE HERE
    dim = len(pos)+1  
    dim2 = len(pos_unknown)    
    
    k = np.zeros(dim)          #vector k C10, C20, ...Cn0
    K = np.zeros((dim, dim))   #Covariance Matrix
    estimated_val = np.zeros(dim2)
    estimated_var = np.zeros(dim2)    
    
    k[dim-1] = 1.0             #ordinary kriging vector
    for i in range(dim-1):     #ordinary kriging matrix
        K[i][dim-1] = 1.0
        K[dim-1][i] = 1.0

    #construct the K covariance matrix, only need to construct it once!    
    dist = cdist(pos, pos, 'wminkowski', p=2, w=weight)    
    
    for i in range(dim-1):
        j = i+1
        while(j<dim-1):
            K[i][j] = cf(dist[i][j], r, sample_var, nugget)
            K[j][i] = K[i][j]
            j+=1    
        K[i][i] = cf(0, r, sample_var, nugget)
    
    for j, p in enumerate(pos_unknown):
        dist = cdist([p], pos, 'wminkowski', p=2, w=weight)[0]    
        for i in range(dim-1):
            k[i] = cf(dist[i], r, sample_var, nugget)    
        w = np.linalg.solve(K, k)
        estimated_val[j] = np.dot(w[:dim-1], vals)
        estimated_var[j] = sample_var - np.dot(w, k)
        
    return estimated_val, estimated_var
    
def SGS(pos, n_neighbour, init_vals, r, sample_mean, sample_var, cf):
    """Perform sampling on path for pos values
    
    **Arguments**:
        - pos = np.array : pos values to obtain samples, positions of known values
                           should be put at start of the array.
                           ex: [known_pos1, known_pos2, unknown_pos1, ....]
        - n_neighbour = int : number of neighbour values to take into account
        - init_vals = np.array : values of initial points, should match known_pos
                                 in pos array.
        - r = float : variogram range
        - sample_mean = float : mean of sample population
        - sample_var = float : variance of sample population
    """
    
    sampled_vals = np.zeros(len(pos))
    val = np.zeros(n_neighbour)
    
    stdev = sample_var**0.5
    known = len(init_vals)  #number of known values      
    
    for i in range(known):
        sampled_vals[i] = init_vals[i]
    
    if(known==0):
        known = 1    
        sampled_vals[0] = np.random.normal(sample_mean, stdev) #1st point
    
    
    for i in range(known, n_neighbour+1):
        est_val, est_var = kriging([pos[i]], pos[0:i], sampled_vals[0:i], r, sample_mean, sample_var, cf)
        sampled_vals[i] = np.random.normal(est_val, est_var**0.5)
    
    if(known <= n_neighbour):
        known = n_neighbour + 1
    
    for i in range(known, len(pos)):
        distance = cdist([pos[i]], pos[0:i], 'euclidean')[0]
        z_pos = []
        for j, k in enumerate(np.argsort(distance)[0:n_neighbour]): #j: from 0 to neighbour-1 k: indices
            z_pos.append(pos[k])
            val[j] = sampled_vals[k]
        
        est_val, est_var = kriging([pos[i]], z_pos, val, r, sample_mean, sample_var, cf)
        sampled_vals[i] = np.random.normal(est_val, est_var**0.5)
    
    return sampled_vals
    
def experimental_vario(sampled_vals, pos, lags, tol):
    """Calculate experimetal variogram for sampled values
    
    **Arguments**:
        - sampled_vals = np.array : sampled data values
        - pos = np.array : positions of values
        - lags = np.array : lags to sample
        - tol = float : tolerance level, should be larger than half of the lag interval
        
    **Returns**:
        - all_semivars
    """
    # YOUR CODE HERE
    all_semivars =  np.zeros(len(lags))
    pair_count = np.zeros(len(lags))
    dist = cdist(pos, pos, 'euclidean')    
    for j in range(len(sampled_vals)):
        zu = sampled_vals[j]
        for h in dist[j]:
            if(h<1e-10 and h>-1e-10):
                continue
            i = np.searchsorted(lags, h) #looking for interval where h belongs to
            if(i>len(lags)): #if not in interval, look for next point
                continue
            if(h<lags[i-1]+tol): 
                all_semivars[i-1] += (sampled_vals[i-1]-zu)**2
                pair_count[i-1] += 1
            if(h>lags[i]-tol):
                all_semivars[i] += (sampled_vals[i]-zu)**2
                pair_count[i] += 1
    
    for i,p in enumerate(pair_count):
        if p==0:
            pair_count[i] = 1  #so we can not having divided by 0 error.
            
    all_semivars = all_semivars / pair_count * 0.5
    return all_semivars

def aniso_vario(sampled_vals, pos, lags, tol, weight):
    """Calculate experimetal variogram for sampled values
    
    **Arguments**:
        - sampled_vals = np.array : sampled data values
        - pos = np.array : positions of values
        - lags = np.array : lags to sample
        - tol = float : tolerance level, should be larger than half of the lag interval
        
    **Returns**:
        - all_semivars
    """
    # YOUR CODE HERE
    all_semivars =  np.zeros(len(lags))
    pair_count = np.zeros(len(lags))
    #weight = np.diag(weight)
    dist = cdist(pos, pos, 'wminkowski', p=2, w=weight)
    
    for j in range(len(sampled_vals)):
        zu = sampled_vals[j]
        for h in dist[j]:
            i = np.searchsorted(lags, h) #looking for interval where h belongs to
            if(i>=len(lags)): #if not in interval, look for next point
                continue
            if(h<lags[i-1]+tol): 
                all_semivars[i-1] += (sampled_vals[i-1]-zu)**2
                pair_count[i-1] += 1
            if(h>lags[i]-tol):
                all_semivars[i] += (sampled_vals[i]-zu)**2
                pair_count[i] += 1
    
    for i,p in enumerate(pair_count):
        if p==0:
            pair_count[i] = 1
            
    all_semivars = all_semivars / pair_count * 0.5
    return all_semivars