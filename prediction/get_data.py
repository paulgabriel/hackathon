import shapefile as shp
import numpy as np
#import fiona
import os
import shutil
import glob

litho_field_name = "ONAME"
x_field_name = "X"
y_field_name = "Y"
z_field_name = "Z"

name_to_id = {
"ts_C2710_k2_06_tolm-teoo_top_04":1405,"ts_C2710_k2_04_tmiu_top_03":1406,"ts_C2710_k2_03_tmim-tpl_top_02":1408,"ts_C2710_k2_05_tolm-tolo_top_04":1407,"ts_C2710_k2_06_tolm-teoo_top_05":1410,"ts_C2710_k2_07_tpao-teou_top_04":1411,"ts_C2710_k2_02_q_top_01":1409,"ts_C2710_k2_07_tpao-teou_top_06":1412,"ts_C2710_k2_10_jo-Wd_top_08":1415,"ts_C2710_k2_08_kro_top_07":1413,"ts_C2710_k2_10_jo-Wd_top_09":1416,"ts_C2710_k2_11_jutco-jmclo_top_09":1417,"ts_C2710_k2_09_kru_top_08":1414,"ts_C2710_k2_11_jutco-jmclo_top_10":1418,"ts_C2710_k2_12_juhe-jutcu_top_09":1419,"ts_C2710_k2_12_juhe-jutcu_top_10":1420,"ts_C2710_k2_12_juhe-jutcu_top_11":1421,"ts_C2710_k2_13_k_top_09":1422,"ts_C2710_k2_13_k_top_10":1423,"ts_C2710_k2_14_so-m_top_10":1425,"ts_C2710_k2_14_so-m_top_11":1426,"ts_C2710_k2_13_k_top_12":1424,"ts_C2710_k2_15_su-sm_top_13":1428,"ts_C2710_k2_14_so-m_top_13":1427,"ts_C2710_k2_16_z_top_14":1430,"ts_C2710_k2_15_su-sm_top_14":1429,"ts_C2710_k2_16_z_top_dach_barkholt_09":1432,"ts_C2710_k2_16_z_top_15":1431,"ts_C2710_k2_16_z_top_dach_barkholt_13":1433,"ts_C2710_k2_16_z_top_dach_bedekaspel_09":1434,"ts_C2710_k2_16_z_top_dach_bedekaspel_10":1435,"ts_C2710_k2_16_z_top_dach_berdum-jever_09":1436,"ts_C2710_k2_16_z_top_dach_etzel_09":1437,"ts_C2710_k2_16_z_top_dach_etzel_11":1438,"ts_C2710_k2_16_z_top_dach_rhaude_08":1439,"ts_C2710_k2_16_z_top_dach_rhaude_09":1440,"ts_C2710_k2_16_z_top_dach_strackholt_09":1441,"ts_C2710_k2_16_z_top_dach_timmel_09":1442,"ts_C2710_k2_16_z_top_dach_westdorf_09":1443,"ts_C2710_k2_16_z_top_dach_wittmund_12":1444,"ts_C2710_k2_16_z_top_dach_zwischenahn_08":1445,"ts_C2714_k2_04_tmiu_top_02":1448,"ts_C2714_k2_05_tolm-tolo_top_02":1450,"ts_C2714_k2_03_tmim-tpl_top_02":1447,"ts_C2714_k2_04_tmiu_top_03":1449,"ts_C2714_k2_02_q_top_01":1446,"ts_C2714_k2_05_tolm-tolo_top_03":1451,"ts_C2714_k2_06_tolm-teoo_top_02":1453,"ts_C2714_k2_05_tolm-tolo_top_04":1452,"ts_C2714_k2_06_tolm-teoo_top_03":1454,"ts_C2714_k2_06_tolm-teoo_top_04":1455,"ts_C2714_k2_06_tolm-teoo_top_05":1456,"ts_C2714_k2_07_tpao-teou_top_02":1457,"ts_C2714_k2_07_tpao-teou_top_03":1458,"ts_C2714_k2_07_tpao-teou_top_04":1459,"ts_C2714_k2_07_tpao-teou_top_05":1460,"ts_C2714_k2_08_kro_top_02":1462,"ts_C2714_k2_07_tpao-teou_top_06":1461,"ts_C2714_k2_08_kro_top_03":1463,"ts_C2714_k2_08_kro_top_04":1464,"ts_C2714_k2_08_kro_top_06":1465,"ts_C2714_k2_08_kro_top_07":1466,"ts_C2714_k2_09_kru_top_08":1467,"ts_C2714_k2_10_jo-Wd_top_09":1468,"ts_C2714_k2_11_jutco-jmclo_top_10":1470,"ts_C2714_k2_11_jutco-jmclo_top_09":1469,"ts_C2714_k2_12_juhe-jutcu_top_10":1472,"ts_C2714_k2_12_juhe-jutcu_top_11":1473,"ts_C2714_k2_12_juhe-jutcu_top_09":1471,"ts_C2714_k2_13_k_top_09":1474,"ts_C2714_k2_13_k_top_10":1475,"ts_C2714_k2_13_k_top_11":1476,"ts_C2714_k2_14_so-m_top_10":1478,"ts_C2714_k2_13_k_top_12":1477,"ts_C2714_k2_14_so-m_top_11":1479,"ts_C2714_k2_15_su-sm_top_11":1481,"ts_C2714_k2_14_so-m_top_13":1480,"ts_C2714_k2_15_su-sm_top_14":1482,"ts_C2714_k2_16_z_top_dach_arngast_09":1484,"ts_C2714_k2_16_z_top_15":1483,"ts_C2714_k2_16_z_top_dach_aschwarden_09":1485,"ts_C2714_k2_16_z_top_dach_dedesdorf_09":1486,"ts_C2714_k2_16_z_top_dach_jaderberg_09":1487,"ts_C2714_k2_16_z_top_dach_neuenhuntorf_08":1488,"ts_C2714_k2_16_z_top_dach_neuenhuntorf_09":1489,"ts_C2714_k2_16_z_top_dach_ruestringen_09":1490,"ts_C2714_k2_16_z_top_dach_seefeld_09":1491,"ts_C2714_k2_16_z_top_dach_spieka_12":1492,"ts_C2714_k2_16_z_top_dach_zwischenahn_08":1493,"ts_C2714_k2_16_z_top_dach_zwischenahn_09":1494,"ts_C3110_k2_04_tmiu_top_02":1497,"ts_C3110_k2_03_tmim-tpl_top_02":1496,"ts_C3110_k2_05_tolm-tolo_top_02":1499,"ts_C3110_k2_04_tmiu_top_03":1498,"ts_C3110_k2_02_q_top_01":1495,"ts_C3110_k2_05_tolm-tolo_top_03":1500,"ts_C3110_k2_06_tolm-teoo_top_02":1502,"ts_C3110_k2_05_tolm-tolo_top_04":1501,"ts_C3110_k2_06_tolm-teoo_top_03":1503,"ts_C3110_k2_06_tolm-teoo_top_04":1504,"ts_C3110_k2_06_tolm-teoo_top_05":1505,"ts_C3110_k2_07_tpao-teou_top_02":1506,"ts_C3110_k2_07_tpao-teou_top_03":1507,"ts_C3110_k2_07_tpao-teou_top_04":1508,"ts_C3110_k2_07_tpao-teou_top_05":1509,"ts_C3110_k2_07_tpao-teou_top_06":1510,"ts_C3110_k2_08_kro_top_07":1511,"ts_C3110_k2_09_kru_top_08":1512,"ts_C3110_k2_10_jo-Wd_top_09":1513,"ts_C3110_k2_11_jutco-jmclo_top_09":1514,"ts_C3110_k2_11_jutco-jmclo_top_10":1515,"ts_C3110_k2_12_juhe-jutcu_top_09":1516,"ts_C3110_k2_12_juhe-jutcu_top_10":1517,"ts_C3110_k2_12_juhe-jutcu_top_11":1518,"ts_C3110_k2_13_k_top_09":1519,"ts_C3110_k2_13_k_top_10":1520,"ts_C3110_k2_13_k_top_12":1521,"ts_C3110_k2_14_so-m_top_09":1522,"ts_C3110_k2_14_so-m_top_10":1523,"ts_C3110_k2_14_so-m_top_13":1524,"ts_C3110_k2_15_su-sm_top_14":1525,"ts_C3110_k2_16_z_top_dach_boerger_09":1527,"ts_C3110_k2_16_z_top_15":1526,"ts_C3110_k2_16_z_top_dach_boerger_10":1528,"ts_C3110_k2_16_z_top_dach_bunde_08":1529,"ts_C3110_k2_16_z_top_dach_gehlenberg_02":1530,"ts_C3110_k2_16_z_top_dach_gehlenberg_03":1531,"ts_C3110_k2_16_z_top_dach_kamperfehn_08":1532,"ts_C3110_k2_16_z_top_dach_kamperfehn_13":1533,"ts_C3110_k2_16_z_top_dach_kamperfehn_14":1534,"ts_C3110_k2_16_z_top_dach_lathen_08":1535,"ts_C3110_k2_16_z_top_dach_lathen_10":1536,"ts_C3110_k2_16_z_top_dach_scharrel_08":1537,"ts_C3110_k2_16_z_top_dach_scharrel_09":1538,"ts_C3110_k2_16_z_top_dach_wahn_09":1539,"ts_C3110_k2_16_z_top_dach_zwischenahn_08":1540,"ts_C3110_k2_16_z_top_dach_zwischenahn_09":1541,"ts_C3114_k2_04_tmiu_top_02":1544,"ts_C3114_k2_05_tolm-tolo_top_02":1546,"ts_C3114_k2_04_tmiu_top_03":1545,"ts_C3114_k2_05_tolm-tolo_top_03":1547,"ts_C3114_k2_02_q_top_01":1542,"ts_C3114_k2_03_tmim-tpl_top_02":1543,"ts_C3114_k2_05_tolm-tolo_top_04":1548,"ts_C3114_k2_06_tolm-teoo_top_02":1549,"ts_C3114_k2_06_tolm-teoo_top_03":1550,"ts_C3114_k2_06_tolm-teoo_top_04":1551,"ts_C3114_k2_07_tpao-teou_top_02":1553,"ts_C3114_k2_06_tolm-teoo_top_05":1552,"ts_C3114_k2_07_tpao-teou_top_03":1554,"ts_C3114_k2_07_tpao-teou_top_05":1555,"ts_C3114_k2_07_tpao-teou_top_06":1556,"ts_C3114_k2_09_kru_top_07":1558,"ts_C3114_k2_08_kro_top_07":1557,"ts_C3114_k2_10_jo-Wd_top_07":1560,"ts_C3114_k2_09_kru_top_08":1559,"ts_C3114_k2_10_jo-Wd_top_08":1561,"ts_C3114_k2_10_jo-Wd_top_09":1562,"ts_C3114_k2_11_jutco-jmclo_top_07":1563,"ts_C3114_k2_11_jutco-jmclo_top_08":1564,"ts_C3114_k2_11_jutco-jmclo_top_09":1565,"ts_C3114_k2_11_jutco-jmclo_top_10":1566,"ts_C3114_k2_12_juhe-jutcu_top_07":1567,"ts_C3114_k2_12_juhe-jutcu_top_08":1568,"ts_C3114_k2_12_juhe-jutcu_top_09":1569,"ts_C3114_k2_12_juhe-jutcu_top_10":1570,"ts_C3114_k2_12_juhe-jutcu_top_11":1571,"ts_C3114_k2_13_k_top_09":1572,"ts_C3114_k2_13_k_top_10":1573,"ts_C3114_k2_13_k_top_11":1574,"ts_C3114_k2_14_so-m_top_10":1576,"ts_C3114_k2_13_k_top_12":1575,"ts_C3114_k2_14_so-m_top_11":1577,"ts_C3114_k2_15_su-sm_top_11":1579,"ts_C3114_k2_14_so-m_top_13":1578,"ts_C3114_k2_15_su-sm_top_14":1580,"ts_C3114_k2_16_z_top_dach_arsten_09":1582,"ts_C3114_k2_16_z_top_15":1581,"ts_C3114_k2_16_z_top_dach_arsten_10":1583,"ts_C3114_k2_16_z_top_dach_arsten_13":1584,"ts_C3114_k2_16_z_top_dach_gruppen-buehren_10":1585,"ts_C3114_k2_16_z_top_dach_gruppen-buehren_13":1586,"ts_C3114_k2_16_z_top_dach_hasbruch_13":1587,"ts_C3114_k2_16_z_top_dach_neuenhuntorf_08":1588,"ts_C3114_k2_16_z_top_dach_neuenhuntorf_09":1589,"ts_C3114_k2_16_z_top_dach_oldenburg-nordwest_10":1590,"ts_C3114_k2_16_z_top_dach_oldenburg-nordwest_13":1591,"ts_C3114_k2_16_z_top_dach_oldenburg-sued_07":1592,"ts_C3114_k2_16_z_top_dach_oldenburg-sued_08":1593,"ts_C3114_k2_16_z_top_dach_oldenburg-sued_10":1594,"ts_C3114_k2_16_z_top_dach_oldenburg-sued_11":1595,"ts_C3114_k2_16_z_top_dach_oldenburg-sued_13":1596,"ts_C3114_k2_16_z_top_dach_Sagermeer_03":1597,"ts_C3114_k2_16_z_top_dach_Sagermeer_06":1598,"ts_C3114_k2_16_z_top_dach_zwischenahn_08":1599,"ts_C3114_k2_16_z_top_dach_zwischenahn_09":1600,"ts_C3114_k2_16_z_top_dach_zwischenahn_13":1601
}

def dist_on_line(p, p0):
    return ((p0[0]-p[0])**2 + (p0[1]-p[1])**2)**0.5 

def get_field_id(fields, name):
    return next((i for i, field in enumerate(fields)
                 if field[0] == name), None) - 1


def get_well_data(file_path, start_point):
    """return numpy matrix for the well data, x,y,z,id"""
    sf = shp.Reader(file_path)
    if sf.shapeType != shp.POINTZ:
        print("shapefile has wrong type")
        return None

    litho_field_id = get_field_id(sf.fields, litho_field_name)
    x_field_id = get_field_id(sf.fields, x_field_name)
    y_field_id = get_field_id(sf.fields, y_field_name)
    z_field_id = get_field_id(sf.fields, z_field_name)

    if litho_field_id is None or x_field_id is None or z_field_id is None:
        print("missing required field")
        return None

    data = sf.records()
    dist = dist_on_line([data[0][x_field_id], data[0][y_field_id]], start_point)

    data_array = [(dist, record[z_field_id],
                   name_to_id[record[litho_field_id]]) for record in data]

    return np.array(data_array, dtype='f8, f8, i4')

def get_wells_data(id, data_path):
    """return all well data belonging to an id, id is (idx,idy)"""

    wells = []

    data_path = os.path.expanduser(data_path)

    id_x = "{0:0>4}".format(id[0])
    id_y = "{0:0>4}".format(id[1])

    file_base = id_x + "_" + id_y + "_"
    borefile_base = file_base + "saltdomeHit_"
    borefile_glob = borefile_base + "*.shp"
    metadata_stub = "metadata.txt"
    metadata_path = os.path.join(data_path, file_base + metadata_stub)
    f = open(metadata_path)
    points = f.read()
    points = points.split('\n')
    section_points = points[0:4]
    section_points = [[float(points[0]), float(points[1])], [float(points[2]), float(points[3])]]
    start_point = section_points[0]
    bore_file_paths = glob.glob(os.path.join(data_path, borefile_glob))

    for bore_file_path in bore_file_paths:
        wells.append(get_well_data(bore_file_path, start_point))

    length = dist_on_line(section_points[1], section_points[0])
    return (wells, length)

'''
def get_section_data(file_path):
    """todo"""
    features = fiona.open(file_path)

    data_array = []
    for feature in features:
        litho_id = name_to_id[feature['properties'][litho_field_name]]
        for coord in feature['geometry']['coordinates']:
            data_array.append((coord[0], coord[1], coord[2], litho_id))
    return np.array(data_array, dtype='f8, f8, f8, i4')
'''