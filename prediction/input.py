#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 10 13:32:43 2017

@author: huang
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from library import *
from lib_krig import krig_class
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.linear_model import LinearRegression

input_x = np.arange(0,5,0.1)
input_y = np.arange(0,5,0.1)
mesh_x, mesh_y = np.meshgrid(input_x, input_y)
mesh = np.array([input_x, input_y])
meshT = mesh.T
input_lith = np.random.rand(50,50)

well_x = np.array([1,1,2,1,1,1,2,2,2,2,4,4,4])
well_y = np.array([5,4.5,5,4,3,2,4,3,2,1,5,4,2.5])
well_pos = np.array([well_x, well_y])
well_pos = well_pos.T
well_lith = np.array([0,0,0,1,2,2,1,2,3,3,1,3,4])

plt.figure()
#plt.pcolormesh(input_x, input_y, input_lith)
plt.scatter(well_x, well_y, c=well_lith, cmap='viridis')

kk = krig_class()
sample_mean = np.mean(well_lith)
sample_var = np.var(well_lith)
r = 2.5
cf_func = cf_gaussian
lags = np.arange(0,5.0,0.4)
tol = 0.25
sill = 0.7
nugget = 0.0

pos_unknown = []
for i in input_x:
    for j in input_y:
        pos_unknown.append([i,j])


kk.variogram(well_pos, well_lith, r, cf_func, lags, tol, sill, nugget)
zz, krig_var = kriging(pos_unknown, well_pos, well_lith, r, sample_mean, sample_var, cf_func, nugget)
        
zz = zz.reshape(len(input_x),len(input_y))
zz = zz.T

plt.figure()
plt.pcolormesh(input_x, input_y, zz, cmap='viridis', vmin=0.0, vmax=np.max(well_lith))
