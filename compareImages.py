from PIL import Image, ImageChops

img1 = 'pngs/0000_0011_section.png'
img2 = 'pngs/0000_0012_section.png'


def imagecompare(imgfile1, imgfile2):

    diffcount = 0.0
    im1 = Image.open(imgfile1)
    im2 = Image.open(imgfile2)

    imgcompdiff = ImageChops.difference(im1, im2)
    diffboundrect = imgcompdiff.getbbox()
    imgdiffcrop = imgcompdiff.crop(diffboundrect)

    pixels1 = im1.getdata()
    pixels2 = im2.getdata()

    pixel_values1 = list(pixels1)

    length = len(pixel_values1)

    pixel_values2 = list(pixels2)

    index = 0
    errors = 0

    black = [(0, 0, 0, 1)]

    for pixel in pixel_values1:
        if pixel != black[0]:

            if pixel != pixel_values2[index]:
                errors = errors + 1

        index = index + 1

    percent = errors / length * 100
    return percent


if __name__ == "__main__":
    print(imagecompare(img1, img2))
    #    seq = []
    #    for row in data:
    #        seq += list(row)

    #    for i in range (0, imgdiffcrop.size[0] * imgdiffcrop.size[1] * 3, 3):
    #        if seq[i] != 0 or seq[i+1] != 0 or seq[i+2] != 0:
    #            diffcount = diffcount + 1.0

    #    saveimg = imgcompdiff.convert('RGB')
    #    saveimg.save('diff/11-12.png')
    #    diffImgLen = imgcompdiff.size[0] * imgcompdiff.size[1] * 1.0
    #    diffpercent = (diffcount * 100) / diffImgLen
    #    return diffpercent
    #except IOError:
    #    print('Input file does not exist')

    #print(imagecompare (img1, img2))
